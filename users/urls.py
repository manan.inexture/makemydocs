from django.urls import include, path
from rest_framework_simplejwt import views as jwt_views
from . import views


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('users/', views.UsersList.as_view(), name='users_list'),
    path('users/<int:pk>/', views.UserDetails.as_view(), name='user_details'),
    path('api_key/generate/', views.ApiKeyGenerate.as_view(), name="api_key_generate"),
    path('users/signin/', views.Signin.as_view(), name="sign_in"),
    path('test/', views.TestView.as_view(), name="test"),
]
