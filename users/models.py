from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime
# Create your models here.


class User(AbstractUser):
    first_name = models.CharField(max_length=50, blank=False)
    last_name = models.CharField(max_length=50, blank=False)
    email = models.EmailField(max_length=50, blank=False)
    phone_number = models.CharField(max_length=14, blank=False)
    active = models.BooleanField(blank=True, null=True)
    auth_token = models.CharField(max_length=254, blank=True, null=True)
    api_key = models.CharField(max_length=64, blank=True, null=True)
    sftp_server = models.CharField(max_length=100, blank=True, null=True)
    sftp_port = models.CharField(max_length=100, blank=True, null=True)
    sftp_username = models.CharField(max_length=100, blank=True, null=True)
    sftp_password = models.CharField(max_length=100, blank=True, null=True)
    created_on = models.DateTimeField(default=datetime.datetime.now)


class ApiKey(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    key = models.CharField(max_length=64, blank=True, null=True)
    active = models.BooleanField(blank=True, null=True)
    created_on = models.DateTimeField(default=datetime.datetime.now)
