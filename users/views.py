from .models import User, ApiKey
from users.serializers import UserSerializer, ApiKeySerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
import random
import string
import requests

class UsersList(APIView):
    """
    List all users, or create new user.
    """
    def get(self, request):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response({"data": serializer.data, "status": status.HTTP_200_OK}, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"data": serializer.data, "status": status.HTTP_201_CREATED}, status=status.HTTP_201_CREATED)
        return Response({"error": serializer.errors, "status": status.HTTP_400_BAD_REQUEST}, status=status.HTTP_400_BAD_REQUEST)


class UserDetails(APIView):
    """
    Retrieve, update or delete a user instance.
    """
    def get_object(self, pk):
        try:
            user = User.objects.get(pk=pk)
            return user
        except User.DoesNotExist:
            return None
        except Exception:
            return None

    def get(self, request, pk):

        user = self.get_object(pk)
        if user:
            serializer = UserSerializer(user)
            return Response({"data": serializer.data, "status": status.HTTP_200_OK}, status=status.HTTP_200_OK)
        return Response({"data": [], "status": status.HTTP_404_NOT_FOUND}, status=status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        user = self.get_object(pk)
        if user:
            serializer = UserSerializer(user, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response({"error": serializer.errors, "status": status.HTTP_400_BAD_REQUEST}, status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": [], "status": status.HTTP_404_NOT_FOUND}, status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, pk):
        user = self.get_object(pk)
        if user:
            user.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response({"data": [], "status": status.HTTP_404_NOT_FOUND}, status=status.HTTP_404_NOT_FOUND)


class ApiKeyGenerate(APIView):
    """
    Generate API_KEY
    """
    def post(self, request):
        try:
            letters_and_digits = string.ascii_letters + string.digits
            api_key = ''.join((random.choice(letters_and_digits) for i in range(64)))
            return Response({"api_key": api_key, "status": status.HTTP_200_OK})
        except Exception:
            return Response({"status": status.HTTP_500_INTERNAL_SERVER_ERROR})


class ApiKeyList(APIView):
    """
    List all API_KEY, or create new API_KEY
    """
    def get(self, request):
        api_keys = ApiKey.objects.all()
        serializer = ApiKeySerializer(api_keys, many=True)
        return Response({"data": serializer.data, "status": status.HTTP_200_OK}, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = ApiKeySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"data": serializer.data, "status": status.HTTP_201_CREATED}, status=status.HTTP_201_CREATED)
        return Response({"error": serializer.errors, "status": status.HTTP_400_BAD_REQUEST}, status=status.HTTP_400_BAD_REQUEST)


class ApiKeyDetails(APIView):
    """
    Retrive, update or delete api_key
    """
    def get_object(self, pk):
        try:
            api_key = ApiKey.objects.get(pk=pk)
            return api_key
        except ApiKey.DoesNotExist:
            return None
        except Exception:
            return None

    def get(self, request, pk):
        api_key = self.get_object(pk)
        if api_key:
            serializer = ApiKeySerializer(api_key)
            return Response({"data": serializer.data, "status": status.HTTP_200_OK}, status=status.HTTP_200_OK)
        return Response({"data": [], "status": status.HTTP_404_NOT_FOUND}, status=status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        api_key = self.get_object(pk)
        if api_key:
            serializer = ApiKeySerializer(api_key, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response({"error": serializer.errors, "status": status.HTTP_400_BAD_REQUEST},
                            status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": [], "status": status.HTTP_404_NOT_FOUND}, status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, pk):
        api_key = self.get_object(pk)
        if api_key:
            api_key.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response({"data": [], "status": status.HTTP_404_NOT_FOUND}, status=status.HTTP_404_NOT_FOUND)


class Signin(APIView):
    """
    Sign in the user
    """
    def post(self, request):
        token_response = requests.post('http://3.214.118.152:8000/api/token/', json=request.data)
        return Response(token_response.json())


class TestView(APIView):

    permission_classes = [IsAuthenticated]
    def get(self, request):
        return Response({"Success": "Success"})