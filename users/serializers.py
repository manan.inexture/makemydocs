from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import ApiKey

UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = UserModel.objects.create_user(**validated_data)
        return user

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            password = validated_data.pop('password')
            instance.set_password(password)

        return super(UserSerializer, self).update(instance, validated_data)

    class Meta:
        model = UserModel
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'phone_number', 'password', 'active', 'auth_token', 'api_key',
                  'sftp_server', 'sftp_port', 'sftp_username', 'sftp_password')


class ApiKeySerializer(serializers.ModelSerializer):

    class Meta:
        model = ApiKey
        fields = ('id', 'user', 'key', 'active', 'created_on')
        depth = 2
