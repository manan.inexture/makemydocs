asgiref==3.2.7
certifi==2020.4.5.1
chardet==3.0.4
Django==2.2
djangorestframework==3.11.0
djangorestframework-simplejwt==4.4.0
idna==2.6
numpy==1.18.4
pandas==1.0.3
psycopg2==2.8.5
psycopg2-binary==2.8.5
pkg-resources==0.0.0
PyJWT==1.7.1
python-dateutil==2.8.1
pytz==2020.1
requests==2.18.4
six==1.14.0
sqlparse==0.3.1
urllib3==1.22
